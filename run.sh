#!/bin/bash -i
ROOT=$(readlink -f $(dirname $0))

pyenv local 2.7.6
python ${ROOT}/parse.py
mv *.json objects/${OCCAM_OBJECT_INDEX}/.
mv objects/${OCCAM_OBJECT_INDEX}/object.json .
