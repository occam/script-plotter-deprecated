import os
import subprocess
import json
import shutil

from occam import Occam

# Gather paths
scripts_path    = os.path.dirname(__file__)
job_path        = os.getcwd()

# Get occam object metadata
object = Occam.load()

# This command will generate the graph for the given input files.
args = ["/bin/bash",
        os.path.join(scripts_path, "run.sh")]

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run SST
Occam.report(command)
