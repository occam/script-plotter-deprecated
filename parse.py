import json
import os
import re
import copy
from occam import Occam
from collections import OrderedDict

object = Occam.load()
configuration = object.configuration("General")

# Gather paths
scripts_path    = os.path.dirname(__file__)
job_path        = os.getcwd()

# Look at inputs
inputs = object.inputs("application/json")

n_files=0
file_list=[]
for input in inputs:
    directory = input.volume()
    object_file = os.path.join(directory,"object.json")
    if os.path.exists(object_file):
        with open(object_file) as obj:
            object_info = json.load(obj)
        output_filename = object_info.get("file","")
        output_file =  os.path.join(directory,output_filename)
        if (output_filename!="") and (os.path.exists(object_file)):
            file_list.append(output_file)
            n_files += 1

plot_type=configuration['plot_type']
plot_mode=configuration['plot_mode']



data_template = OrderedDict({
	"x": [],
	"y": [],
	"name": "",
	"type": plot_type,
	"mode": plot_mode
})

output_template = OrderedDict({
	"data" : [],
	"layout" : 	OrderedDict({
		"title" : "",
		"xaxis":OrderedDict({
			"title":""
        }),
		"yaxis":OrderedDict({
			"title":""
		})
	})
})

output = output_template.copy()
i=0

number_prog = re.compile("\d+")
group_selection = configuration['group_selection']
group_selection_strings = group_selection.split("AND")
y_axis_selection = configuration['y_axis_selection']
x_axis_selection = configuration['x_axis_selection']

chart_title = configuration['plot_title'].replace(" ","_")
x_label = configuration['x_label']
y_label = configuration['y_label']

groups={}
if (n_files != 0):
    for file in file_list:
        with open(file) as iterator:
            json_data = json.load(iterator)

        for experiment in json_data.get("experiment",{}):
            # Get in which group to put this experiment
            group_name = ""
            for group_string in group_selection_strings:
                group_value = experiment
                group_string=group_string.replace(" ","")
                for key in group_string.replace(" ","").split("::"):
                    prog = re.compile("\[\d+\]") # Example 'input' or 'input[0]'
                    all_array_positions=prog.findall(key) # Example   ['[0]']
                    if(len(all_array_positions)>0):
                        last_array_position=all_array_positions[-1] # Example '[0]'
                        array_position=int(last_array_position[1:-1]) # Example 0
                        key = key[0:-len(last_array_position)] # example 'input'
                        group_value = group_value[key][array_position] if( ( key in group_value) and (len(group_value[key])>array_position) ) else {}
                    else:
                        group_value = group_value[key] if key in group_value else {}
                        if isinstance(group_value, list): group_value=group_value[0]

                group_value_units=experiment
                for key in ("%s_OCCAM_unit"%(group_string)).split("::"):
                    prog = re.compile("\[\d+\]")
                    all_array_positions=prog.findall(key) # Example   ['[0]']
                    if(len(all_array_positions)>0):
                        last_array_position=all_array_positions[-1] # Example '[0]'
                        array_position=int(last_array_position[1:-1]) # Example 0
                        key = key[0:-len(last_array_position)] # example 'input'
                        group_value_units = group_value_units[key][array_position] if( ( key in group_value_units) and (len(group_value_units[key])>array_position) ) else {}
                    else:
                        group_value_units = group_value_units[key] if key in group_value_units else {}
                        if isinstance(group_value_units, list): group_value_units=group_value_units[0]

                if(group_name==""):
                    group_name="%s.%s=%s"%(group_string.split("::")[-2],group_string.split("::")[-1],group_value)
                else:
                    group_name="%s\n%s.%s=%s"%(group_name,group_string.split("::")[-2],group_string.split("::")[-1],group_value)
                if(group_value_units!={}):
                    group_name="%s (%s)"%(group_name,group_value_units)

            group=groups.get(group_name, copy.deepcopy(data_template))


            # Get the Y value
            y_data=experiment
            for key in y_axis_selection.split("::"):
                prog = re.compile("\[\d+\]")
                all_array_positions=prog.findall(key) # Example   ['[0]']
                if(len(all_array_positions)>0):
                    last_array_position=all_array_positions[-1] # Example '[0]'
                    array_position=int(last_array_position[1:-1]) # Example 0
                    key = key[0:-len(last_array_position)] # example 'input'
                    y_data = y_data[key][array_position] if( ( key in y_data) and (len(y_data[key])>array_position) ) else {}

                else:
                    y_data = y_data[key] if key in y_data else {}
                    if isinstance(y_data, list): y_data=y_data[0]
            if(~isinstance(y_data,int)):
                y_data=number_prog.findall(y_data)[-1] or len(group["y"])
            group["y"].append(y_data)

            # Get the X value
            x_data=experiment
            for key in x_axis_selection.split("::"):
                prog = re.compile("\[\d+\]")
                all_array_positions=prog.findall(key) # Example   ['[0]']
                if(len(all_array_positions)>0):
                    last_array_position=all_array_positions[-1] # Example '[0]'
                    array_position=int(last_array_position[1:-1]) # Example 0
                    key = key[0:-len(last_array_position)] # example 'input'
                    x_data = x_data[key][array_position] if( ( key in x_data) and (len(x_data[key])>array_position) ) else {}
                else:
                    x_data = x_data[key] if key in x_data else {}
                    if isinstance(x_data, list): x_data=x_data[0]
            if(~isinstance(x_data,int)):
                x_data=number_prog.findall(x_data)[-1] or len(group["x"])
            group["x"].append(x_data)

            group["name"]=group_name
            group["type"]="bar"
            groups[group_name]=group
            print(groups)
    for key,data in groups.items():
        output["data"].append(data)
        output["layout"]["title"] = chart_title.replace('_', ' ');
        output["layout"]["xaxis"]["title"] = x_label
        output["layout"]["yaxis"]["title"] = y_label
    with open(chart_title + '.json', 'w') as f:
        json.dump(output, f, sort_keys=True, indent=4)
